class TagsController < ApplicationController
	before_action :require_login, only: [:destroy]

	def index
		@tags = Tag.all 
	end

	def show
		@tag = Tag.find(params[:id])
	end

	def edit
		@tag = Tag.find(params[:id])
	end

	def destroy
		@tag = Tag.find(params[:id])
		@tag.destroy
		flash.notice = "Deleted tag!"
		redirect_to tags_path
	end

	def update
		@tag = Tag.find(params[:id])
		@tag.update(tag_params)
		flash.notice = "Updated tag!"
		redirect_to tags_path
	end



	private

	def tag_params
		params.require(:tag).permit(:name)
	end

end
