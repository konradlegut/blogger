Article.destroy_all
Comment.destroy_all


20.times do
	Comment.create(author_name: Faker::Name.name, body: Faker::Lorem.sentence)
	Article.create(title: Faker::Name.title, body: Faker::Lorem.paragraph(20), image: Faker::Avatar.image)
end